package com.leegality.encryptionDecryptionUtil.util;

import com.leegality.encryptionDecryptionUtil.keystore.LeegalityKeyStore;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;

public class SignatureUtil {

    private SignatureUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    private static LeegalityKeyStore buildKeyStore(InputStream source, String password) {
        return LeegalityKeyStore.builder()
                .stream(source)
                .storePassword(password)
                .build();
    }

    private static LeegalityKeyStore buildKeyStore(InputStream source, String password, String keyStoreType) {
        return LeegalityKeyStore.builder()
                .stream(source)
                .storePassword(password)
                .keyStore(keyStoreType)
                .build();
    }

    public static LeegalityKeyStore buildKeyStore(String source, String password) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(source);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return buildKeyStore(inputStream, password);
    }

    public static LeegalityKeyStore buildKeyStore(String source, String password, String keyStoreType) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(source);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return buildKeyStore(inputStream, password, keyStoreType);
    }

//    public static InputStream stringToInputStream(String pfxData) {
//        return new ByteArrayInputStream();
//    }

    public static PublicKey getPublicKey(String source, String password) {
        LeegalityKeyStore leegalityKeyStore = buildKeyStore(source, password);
        return leegalityKeyStore.getPublicKey();
    }

    public static PrivateKey getPrivateKey(String source, String password) {
        LeegalityKeyStore leegalityKeyStore = buildKeyStore(source, password);
        return leegalityKeyStore.getPrivateKey();
    }

    public static PrivateKey getPrivateKey(String source, String password, String keyStoreType) {
        LeegalityKeyStore leegalityKeyStore = buildKeyStore(source, password,keyStoreType);
        return leegalityKeyStore.getPrivateKey();
    }

    public static X509Certificate getCertificate(String source, String password) {
        LeegalityKeyStore leegalityKeyStore = buildKeyStore(source, password);
        return leegalityKeyStore.getX509Certificate();
    }

    public static PublicKey getPublicKey(String pubKeyPath) throws Exception {
        FileInputStream fis = new FileInputStream(pubKeyPath);
        try {
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            X509Certificate cert = (X509Certificate) certFactory
                    .generateCertificate(fis);
            return cert.getPublicKey();
        } catch (CertificateException e) {
            throw new Exception("Public certificate not valid");
        }
    }
}
