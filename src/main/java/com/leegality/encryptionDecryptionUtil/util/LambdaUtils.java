package com.leegality.encryptionDecryptionUtil.util;


import com.leegality.encryptionDecryptionUtil.exception.SignatureException;

public final class LambdaUtils {

    private LambdaUtils() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static <T> T wrapException(Wrapper<T> wrapper) {
        try {
            return wrapper.execute();
        } catch (Exception ex) {
            throw new SignatureException(ex);
        }
    }

    public static void wrapException(VoidWrapper wrapper) {
        try {
            wrapper.execute();
        } catch (Exception ex) {
            throw new SignatureException(ex);
        }
    }

    public static void suppressException(VoidWrapper wrapper) {
        try {
            wrapper.execute();
        } catch (Exception ex) {
            System.out.println("Error while executing tasks");
        }
    }


    public interface Wrapper<T> {
        T execute() throws Exception;
    }

    public interface VoidWrapper {
        void execute() throws Exception;
    }
}
