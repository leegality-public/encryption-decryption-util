package com.leegality.encryptionDecryptionUtil.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;

public class EncryptionUtil {
    public static final String RSA_ECB_PKCS_1_PADDING = "RSA/ECB/PKCS1Padding";

    private EncryptionUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static String encrypt(String rawData, PublicKey publicKey) throws GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(RSA_ECB_PKCS_1_PADDING);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encVal = cipher.doFinal(Base64.decodeBase64(rawData));
        return Base64.encodeBase64String(encVal);
    }


    public static String decrypt(String rawData, PrivateKey privateKey) throws GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(RSA_ECB_PKCS_1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decValue = cipher.doFinal(Base64.decodeBase64(rawData));
        return Base64.encodeBase64String(decValue);
    }


}
