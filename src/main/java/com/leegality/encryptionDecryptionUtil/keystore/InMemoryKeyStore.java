package com.leegality.encryptionDecryptionUtil.keystore;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

final class InMemoryKeyStore implements LeegalityKeyStore {
    private final PrivateKey privateKey;
    private final Certificate[] certificateChain;
    private final PublicKey publicKey;
    private final X509Certificate x509Certificate;
    private final String pdfSigningAlgorithm;

    @Override
    public PrivateKey getPrivateKey() {
        return this.privateKey;
    }

    @Override
    public Certificate[] getCertificateChain() {
        return this.certificateChain.clone();
    }

    @Override
    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    @Override
    public X509Certificate getX509Certificate() {
        return this.x509Certificate;
    }

    @Override
    public String getPdfSigningAlgorithm() {
        return this.pdfSigningAlgorithm;
    }

    public InMemoryKeyStore(PrivateKey privateKey, Certificate[] certificateChain, PublicKey publicKey,
                            X509Certificate x509Certificate, String pdfSigningAlgorithm) {
        this.privateKey = privateKey;
        this.certificateChain = certificateChain.clone();
        this.publicKey = publicKey;
        this.x509Certificate = x509Certificate;
        this.pdfSigningAlgorithm = pdfSigningAlgorithm;
    }
}
