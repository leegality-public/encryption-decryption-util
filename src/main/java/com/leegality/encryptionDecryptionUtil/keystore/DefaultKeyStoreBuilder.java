package com.leegality.encryptionDecryptionUtil.keystore;


import com.leegality.encryptionDecryptionUtil.util.LambdaUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

final class DefaultKeyStoreBuilder implements LeegalityKeyStoreBuilder {
    private InputStream inputStream;
    private String storePassword;
    private String keyPassword;
    private String alias;
    private String keyStoreType;
    private String providerPrefix;
    private String pdfSigningAlgorithm = "SHA256WithRSA";

    @Override
    public LeegalityKeyStoreBuilder stream(InputStream stream) {
        Objects.requireNonNull(stream, "Keystore stream should not be null.");
        this.inputStream = stream;
        return this;
    }

    @Override
    public LeegalityKeyStoreBuilder storePassword(String storePassword) {
        this.storePassword = storePassword;
        return this;
    }

    @Override
    public LeegalityKeyStoreBuilder keyStore(String keyStoreType) {
        this.keyStoreType = keyStoreType;
        return this;
    }
    @Override
    public LeegalityKeyStore build() {
        Objects.requireNonNull(this.inputStream, "KeyStore location is missing.");
        String ksType = this.keyStoreType != null && !this.keyStoreType.isEmpty() ? this.keyStoreType : KeyStore.getDefaultType();
        String provider = this.providerPrefix != null && !this.providerPrefix.isEmpty() ? this.providerPrefix : "BC";
        char[] storePassword = this.storePassword != null && !this.storePassword.isEmpty() ? this.storePassword.toCharArray() : new char[0];
        char[] keyPassword = this.keyPassword != null && !this.keyPassword.isEmpty() ? this.keyPassword.toCharArray() : new char[0];

        InMemoryKeyStore inMemoryKeyStore = null;
        try {
            KeyStore keyStore = LambdaUtils.wrapException(() -> KeyStore.getInstance(ksType, provider));
            LambdaUtils.wrapException(() -> {
                keyStore.load(this.inputStream, storePassword);
            });
            List<String> list = Collections.list(keyStore.aliases());
            String alias = this.alias != null && !this.alias.isEmpty() ? this.alias : LambdaUtils.wrapException(() -> list.stream()
                    .filter(stringEnumeration ->
                            LambdaUtils.wrapException(() -> keyStore.getCertificateChain(stringEnumeration)) != null)
                    .findFirst()
                    .get());

            Certificate[] certificateChain = LambdaUtils.wrapException(() -> keyStore.getCertificateChain(alias));
            X509Certificate certificate = LambdaUtils.wrapException(() -> (X509Certificate) keyStore.getCertificate(alias));
            PublicKey publicKey = certificate.getPublicKey();
            PrivateKey privateKey = LambdaUtils.wrapException(() -> (PrivateKey) keyStore.getKey(alias, keyPassword));
            inMemoryKeyStore = new InMemoryKeyStore(privateKey, certificateChain,
                    publicKey, certificate, this.pdfSigningAlgorithm);
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            LambdaUtils.suppressException(() -> {
                this.inputStream.close();
            });
        }

        return inMemoryKeyStore;
    }

    static {
        Security.insertProviderAt(new BouncyCastleProvider(), 0);
    }
}
