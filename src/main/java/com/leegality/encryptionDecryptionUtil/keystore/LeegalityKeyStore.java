package com.leegality.encryptionDecryptionUtil.keystore;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public interface LeegalityKeyStore {
    Certificate[] getCertificateChain();

    X509Certificate getX509Certificate();

    PublicKey getPublicKey();

    PrivateKey getPrivateKey();

    String getPdfSigningAlgorithm();

    static LeegalityKeyStoreBuilder builder() {
        return new DefaultKeyStoreBuilder();
    }
}
