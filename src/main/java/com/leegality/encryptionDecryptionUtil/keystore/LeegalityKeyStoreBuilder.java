package com.leegality.encryptionDecryptionUtil.keystore;

import java.io.InputStream;

public interface LeegalityKeyStoreBuilder {

    LeegalityKeyStoreBuilder stream(InputStream var1);

    LeegalityKeyStoreBuilder storePassword(String var1);

    LeegalityKeyStoreBuilder keyStore(String keyStoreType);

    LeegalityKeyStore build();
}

