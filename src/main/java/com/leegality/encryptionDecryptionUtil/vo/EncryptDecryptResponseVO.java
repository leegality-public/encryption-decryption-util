package com.leegality.encryptionDecryptionUtil.vo;

public class EncryptDecryptResponseVO {

    String payload;
    String salt;

    public EncryptDecryptResponseVO(String payload, String salt) {
        this.payload = payload;
        this.salt = salt;
    }

    public String getPayload() {
        return payload;
    }

    public String getSalt() {
        return salt;
    }

    @Override
    public String toString() {
        return "EncryptDecryptResponseVO{" +
                "payload='" + payload + '\'' +
                ", salt='" + salt + '\'' +
                '}';
    }
}
