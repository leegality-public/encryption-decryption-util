package com.leegality.encryptionDecryptionUtil.key;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Arrays;

public class Key {

    int keySizeInBits = 256;
    int keySize = keySizeInBits / 8;
    int ivSize = 16;
    String keyAlgo = "AES";
    byte[] encoded;

    byte[] initilizeKey() throws GeneralSecurityException {
        byte[] keyData = new byte[keySize + ivSize];
        KeyGenerator keyGenerator = KeyGenerator.getInstance(keyAlgo);
        keyGenerator.init(keySizeInBits);
        SecretKey key = keyGenerator.generateKey();
        byte[] keyBytes = key.getEncoded();
        System.arraycopy(keyBytes, 0, keyData, 0, keyBytes.length);

        byte[] iv = new byte[ivSize];
        new SecureRandom().nextBytes(iv);
        System.arraycopy(iv, 0, keyData, keySize, iv.length);
        return keyData;
    }

    public Key() throws GeneralSecurityException {
        this.encoded = initilizeKey();
    }

    public byte[] getKey() {
        return Arrays.copyOfRange(encoded, 0, keySize);
    }

    public byte[] getIv() {
        return Arrays.copyOfRange(encoded, keySize, encoded.length);
    }

    public byte[] getEncoded() {
        return Arrays.copyOfRange(encoded, 0, encoded.length);
    }

    public String getKeyAlgo() {
        return keyAlgo;
    }
}
