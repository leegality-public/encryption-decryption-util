package com.leegality.encryptionDecryptionUtil.exception;

public class SignatureException extends RuntimeException {

    public SignatureException(String message) {
        super(message);
    }

    public SignatureException(Throwable cause) {
        super(cause);
    }
}
