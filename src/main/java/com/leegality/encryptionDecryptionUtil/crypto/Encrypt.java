package com.leegality.encryptionDecryptionUtil.crypto;

import com.leegality.encryptionDecryptionUtil.key.Key;
import com.leegality.encryptionDecryptionUtil.util.EncryptionUtil;
import com.leegality.encryptionDecryptionUtil.util.SignatureUtil;
import com.leegality.encryptionDecryptionUtil.vo.EncryptDecryptResponseVO;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.PublicKey;
import java.util.Base64;

public class Encrypt {

    private final String algorithm = "AES/CBC/PKCS5Padding";

    public EncryptDecryptResponseVO encrypt(String payloadPath, String publicKeyPath) throws Exception {
        if (payloadPath == null || publicKeyPath == null) {
            throw new Exception("Both payload Path and public key are required.");
        }

        byte[] inputBytes = Files.readAllBytes(Path.of(System.getProperty("user.dir") + payloadPath));
        String keyPath = System.getProperty("user.dir") + publicKeyPath;

        Key key = new Key();
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getKey(), key.getKeyAlgo()),
                new IvParameterSpec(key.getIv()));

        byte[] cipherText = cipher.doFinal(inputBytes);

        String salt = Base64.getEncoder().encodeToString(key.getEncoded());

        PublicKey publicKey = SignatureUtil.getPublicKey(keyPath);
        return new EncryptDecryptResponseVO(Base64.getEncoder().encodeToString(cipherText), EncryptionUtil.encrypt(salt, publicKey));
    }
}
