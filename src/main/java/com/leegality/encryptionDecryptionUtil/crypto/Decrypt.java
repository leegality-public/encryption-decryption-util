package com.leegality.encryptionDecryptionUtil.crypto;

import com.leegality.encryptionDecryptionUtil.util.EncryptionUtil;
import com.leegality.encryptionDecryptionUtil.util.SignatureUtil;
import com.leegality.encryptionDecryptionUtil.vo.EncryptDecryptResponseVO;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.Base64;

public class Decrypt {

    private final String algorithm = "AES/CBC/PKCS5Padding";

    public EncryptDecryptResponseVO decrypt(String payload, String salt, String pfxFilePath, String password,
                                            String keyStoreType) throws IOException, GeneralSecurityException {

        pfxFilePath = System.getProperty("user.dir") + pfxFilePath;
        PrivateKey privateKey;

        if (keyStoreType != null) {
            privateKey = SignatureUtil.getPrivateKey(pfxFilePath, password, keyStoreType);
        } else {
            privateKey = SignatureUtil.getPrivateKey(pfxFilePath, password);
        }

        String keyString = EncryptionUtil.decrypt(salt, privateKey);
        byte[] keyByte = Base64.getDecoder().decode(keyString.getBytes());
        byte[] actKey = Arrays.copyOfRange(keyByte, 0, 32);
        byte[] iv = Arrays.copyOfRange(keyByte, 32, 48);

        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(actKey, algorithm), new IvParameterSpec(iv));
        byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(payload));
        return new EncryptDecryptResponseVO(new String(plainText), null);
    }
}
